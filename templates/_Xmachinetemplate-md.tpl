{{- define "XMachineTemplate-MD" }}
{{- $envAll := index . 0 }}
{{- $machine_deployment_name := index . 1 }}
{{- $machine_deployment_def := index . 2 }}
{{- $labels := index . 3 }}
---
{{ if eq $machine_deployment_def.infra_provider "capd" }}
apiVersion: {{ $envAll.Values.apiVersions.DockerMachineTemplate }}
kind: DockerMachineTemplate
metadata:
  name: {{ $envAll.Values.name }}-md-{{ $machine_deployment_name }}-{{ include "DockerMachineTemplateSpec" $envAll | sha1sum | trunc 10 }}
  namespace: {{ $envAll.Release.Namespace }}
  labels:
    role: worker
{{ $labels | toYaml | indent 4 }}
spec:
  template:
    spec:
{{ include "DockerMachineTemplateSpec" $envAll | indent 6 }}
{{ else if eq $machine_deployment_def.infra_provider "capo" }}
apiVersion: {{ $envAll.Values.apiVersions.OpenStackMachineTemplate }}
kind: OpenStackMachineTemplate
metadata:
  name: {{ $envAll.Values.name }}-md-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_deployment_def.network_interfaces $machine_deployment_def.capo | include "OpenStackMachineTemplateSpec-MD" | sha1sum | trunc 10 }}
  namespace: {{ $envAll.Release.Namespace }}
  labels:
    role: worker
{{ $labels | toYaml | indent 4 }}
spec:
  template:
    spec:
{{ tuple $envAll $machine_deployment_def.network_interfaces $machine_deployment_def.capo | include "OpenStackMachineTemplateSpec-MD" | indent 6 }}
{{ else if eq $machine_deployment_def.infra_provider "capv" }}
apiVersion: {{ $envAll.Values.apiVersions.VSphereMachineTemplate }}
kind: VSphereMachineTemplate
metadata:
  name: {{ $envAll.Values.name }}-md-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_deployment_def.network_interfaces $machine_deployment_def.capv | include "VSphereMachineTemplateSpec-MD" | sha1sum | trunc 10 }}
  namespace: {{ $envAll.Release.Namespace }}
  labels:
    role: worker
{{ $labels | toYaml | indent 4 }}
spec:
  template:
    spec:
{{ tuple $envAll $machine_deployment_def.network_interfaces $machine_deployment_def.capv | include "VSphereMachineTemplateSpec-MD" | indent 6 }}
{{ else if eq $machine_deployment_def.infra_provider "capm3" }}
apiVersion: {{ $envAll.Values.apiVersions.Metal3MachineTemplate }}
kind: Metal3MachineTemplate
metadata:
  name: {{ $envAll.Values.name }}-md-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_deployment_name $machine_deployment_def.network_interfaces $machine_deployment_def.capm3 | include "Metal3MachineTemplateSpec-MD" | include "Metal3MachineTemplateSpec-remove-url-hostname" | sha1sum | trunc 10 }}
  namespace: {{ $envAll.Release.Namespace }}
  labels:
    role: worker
{{ $labels | toYaml | indent 4 }}
spec:
{{ tuple $envAll $machine_deployment_name $machine_deployment_def.network_interfaces $machine_deployment_def.capm3 | include "Metal3MachineTemplateSpec-MD" | indent 2 }}
---
{{ tuple $envAll $machine_deployment_name $machine_deployment_def.network_interfaces $machine_deployment_def.capm3.provisioning_pool_interface $machine_deployment_def.capm3.public_pool_interface $machine_deployment_def.capm3 | include "Metal3DataTemplate-MD" }}
{{ end }}
{{- end }}
