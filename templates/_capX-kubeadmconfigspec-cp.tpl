{{- define "capd-kubeadmConfigSpec-CP" }}
initConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      eviction-hard: nodefs.available<0%,nodefs.inodesFree<0%,imagefs.available<0%
      register-with-taints: ""
joinConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      eviction-hard: nodefs.available<0%,nodefs.inodesFree<0%,imagefs.available<0%
      register-with-taints: ""
clusterConfiguration:
  apiServer:
    certSANs:
      - localhost
      - 127.0.0.1
  controllerManager:
    extraArgs:
      enable-hostpath-provisioner: "true"
ntp: {}
preKubeadmCommands:
  - echo "fs.inotify.max_user_watches = 524288" >> /etc/sysctl.conf
  - echo "fs.inotify.max_user_instances = 512" >> /etc/sysctl.conf
  - sysctl --system
  - |{{ include "containerd-config.toml-registry-config" . | nindent 4 }}
  - systemctl restart containerd.service
  - systemctl daemon-reload && systemctl restart containerd.service
files: []
postKubeadmCommands:
  # dummy; added to cover for the case of empty list of commands
  - export KUBECONFIG=/etc/kubernetes/admin.conf
{{- end }}

{{- define "capo-kubeadmConfigSpec-CP" }}
initConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      provider-id: openstack:///{{`{{ ds.meta_data.uuid }}`}}
    name: {{`'{{ ds.meta_data.name }}'`}}
joinConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      provider-id: openstack:///{{`{{ ds.meta_data.uuid }}`}}
    name: {{`'{{ ds.meta_data.name }}'`}}
# TODO: check for CAPO why there's no clusterConfiguration
clusterConfiguration: {}
ntp: {}
preKubeadmCommands:
  - echo "fs.inotify.max_user_watches = 524288" >> /etc/sysctl.conf
  - echo "fs.inotify.max_user_instances = 512" >> /etc/sysctl.conf
  - sysctl --system
  - |{{ include "containerd-config.toml-registry-config" . | nindent 4 }}
  - systemctl restart containerd.service
  - systemctl daemon-reload && systemctl restart containerd.service
files: []
postKubeadmCommands:
  # dummy; added to cover for the case of empty list of commands
  - export KUBECONFIG=/etc/kubernetes/admin.conf
users: []
{{- end }}

{{- define "capv-kubeadmConfigSpec-CP" }}
initConfiguration:
  nodeRegistration:
    criSocket: /var/run/containerd/containerd.sock
    kubeletExtraArgs:
      cloud-provider: external
    name: {{`'{{ ds.meta_data.hostname }}'`}}
joinConfiguration:
  nodeRegistration:
    criSocket: /var/run/containerd/containerd.sock
    kubeletExtraArgs:
      cloud-provider: external
    name: {{`'{{ ds.meta_data.hostname }}'`}}
clusterConfiguration:
  apiServer:
    certSANs:
      - {{ .Values.cluster_external_ip }}
    extraArgs:
      cloud-provider: external
  controllerManager:
    extraArgs:
      cloud-provider: external
ntp: {}
preKubeadmCommands:
  - hostname {{`"{{ ds.meta_data.hostname }}"`}}
  - echo "::1         ipv6-localhost ipv6-loopback" >/etc/hosts
  - echo "127.0.0.1   localhost" >>/etc/hosts
  - echo "127.0.0.1  {{`{{ ds.meta_data.hostname }}`}}" >>/etc/hosts
  - echo {{`"{{ ds.meta_data.hostname }}"`}} >/etc/hostname
  - update-ca-certificates
files: []
postKubeadmCommands:
  # dummy; added to cover for the case of empty list of commands
  - export KUBECONFIG=/etc/kubernetes/admin.conf
users:
  - name: capv
    sshAuthorizedKeys:
      - {{ .Values.capv.ssh_key }}
    sudo: ALL=(ALL) NOPASSWD:ALL
{{- end }}

{{- define "capm3-kubeadmConfigSpec-CP" }}
initConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      provider-id: metal3:///{{`{{ ds.meta_data.providerid }}`}}
    name: {{`'{{ ds.meta_data.local_hostname }}'`}}
joinConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      provider-id: metal3:///{{`{{ ds.meta_data.providerid }}`}}
    name: {{`'{{ ds.meta_data.local_hostname }}'`}}
clusterConfiguration:
  apiServer:
    extraArgs:
      cloud-provider: baremetal
  controllerManager:
    extraArgs:
      cloud-provider: baremetal
ntp: {}
preKubeadmCommands:
  # dummy; added to cover for the case of empty list of commands
  - hostname {{`"{{ ds.meta_data.local_hostname }}"`}}
  {{- if .Values.enable_longhorn }}
  - | {{ include "shell-longhorn-mounts" . | nindent 4 }}
  {{- end }}
files: []
postKubeadmCommands:
  # dummy; added to cover for the case of empty list of commands
  - export KUBECONFIG=/etc/kubernetes/admin.conf
users: []
{{- end }}
