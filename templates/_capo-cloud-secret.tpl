{{- define "OpenStackIdentityReferenceSecret" -}}
{{- $envAll := index . 0 -}}
{{- $identity_ref_secret_name := index . 1 -}}
{{- $identity_ref_secret_clouds_yaml := index . 2 -}}
apiVersion: v1
kind: Secret
metadata:
  name: {{ $identity_ref_secret_name }}
  labels:
data:
{{ if hasKey $envAll.Values.capo "cacert" -}}
  cacert: {{ $envAll.Values.capo.cacert | b64enc }}
{{- end }}
  clouds.yaml: {{ $identity_ref_secret_clouds_yaml | toYaml | b64enc }}
{{- end -}}
